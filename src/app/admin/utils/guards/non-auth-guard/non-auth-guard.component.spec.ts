import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonAuthGuardComponent } from './non-auth-guard.component';

describe('NonAuthGuardComponent', () => {
  let component: NonAuthGuardComponent;
  let fixture: ComponentFixture<NonAuthGuardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonAuthGuardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonAuthGuardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
