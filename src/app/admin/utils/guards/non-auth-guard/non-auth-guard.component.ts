import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-non-auth-guard',
  templateUrl: './non-auth-guard.component.html',
  styleUrls: ['./non-auth-guard.component.scss']
})
export class NonAuthGuardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
