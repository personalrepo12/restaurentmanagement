import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { MainComponent } from './layouts/main/main.component';
import { PlainComponent } from './layouts/plain/plain.component';
import { HeaderComponent } from './layouts/main/header/header.component';
import { FooterComponent } from './layouts/main/footer/footer.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuardComponent } from './utils/guards/auth-guard/auth-guard.component';
import { NonAuthGuardComponent } from './utils/guards/non-auth-guard/non-auth-guard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { MenuSideBarComponent } from './layouts/main/menu-side-bar/menu-side-bar.component';
import { LoginComponent } from './pages/login/login.component';

;



@NgModule({
  
  declarations: [MainComponent, PlainComponent,

   //Main Component// 
    HeaderComponent, 
    FooterComponent, 
    MenuSideBarComponent,


    //Pages Component//
    DashboardComponent, 
    AuthGuardComponent, 
    NonAuthGuardComponent,PageNotFoundComponent, LoginComponent],


  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AdminModule { }
