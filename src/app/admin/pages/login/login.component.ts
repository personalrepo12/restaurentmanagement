import { Component, OnInit } from '@angular/core';
import { Router, Event } from '@angular/router';
import { UrlManager } from '../../utils/constants.model';
import { InputValidator } from '../../utils/input.validator';
declare var $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
constructor(private router: Router) { }
urlMngr: UrlManager;
email:any = "";
password:any = "";
cnfPassword:any = "";
  ngOnInit(): void {
    $(document).ready(function(){
      $('#forgetPasswordbtn').click(function(){
        $('#loginForm').hide();
        $('#forgetpassword').show();
      });
      $('#cancel_reset').click(function(){
        $('#loginForm').show();
        $('#forgetpassword').hide();
      });
    $('#showPasswordbtn').click(function(){
        $('#hidepasswordsec').hide();
        $('#viewpasswordsec').show();
        $('#showPasswordbtn').hide();
        $('#hidePasswordbtn').show();
      });
      $('#hidePasswordbtn').click(function(){
        $('#hidepasswordsec').show();
        $('#viewpasswordsec').hide();
        $('#hidePasswordbtn').hide();
        $('#showPasswordbtn').show();
      });
    });
  }

  validationLogin(){
    let msg: Array<any> = [];
    if (InputValidator.trim(this.email) == "" || InputValidator.trim(this.password) == ""){
      msg.push("Email and Password is required.");
     console.log("validation Message :",msg);
    }
    else{
      if(this.email == "ak123@gmail.com" || this.password == "12345"){
        msg.push("Login Successfull");
        this.router.navigate([UrlManager.pageUrls.DASHBOARD]);
      }
      if(this.email != "ak123@gmail.com" || this.password != "12345"){
        msg.push("Invalid Credential");
      }
    }
    if(msg.length > 0){
      return msg;
    }
  }

  validationReset(){
    let msg:Array<any> = [];
    if(InputValidator.trim(this.email) == "" 
    ||InputValidator.trim(this.password) == "" 
    ||InputValidator.trim(this.cnfPassword) == ""){
      msg.push("All fields are required.");
    }
    else{
      if(this.password != this.cnfPassword){
        msg.push("Password and Confirm Password should be same.")
      }
      else{
        msg.push("You are ready to go.");
      }
    }
    if(msg.length>0){
      return msg;
    }
  }

  onClickLogin(){
    let validationMessages: Array<any> = this.validationLogin();
    if (validationMessages.length > 0) {
      let msgs: string = validationMessages.join("<br>");
      alert(msgs);
      return;
    }
  }

  onClickReset(){
    let validationMSg:Array<any> = this.validationReset();
    
      if(validationMSg.length>0){
        let msg:string = validationMSg.join("<br/>");
        alert(msg);
        return;
      }
     
    
    
  }

}
